//
//  RegAccountView.m
//  uLike
//
//  Created by mac on 3/7/15.
//  Copyright (c) 2015 mac. All rights reserved.
//

#import "RegAccountView.h"
#import "ColorConvertor.h"
#import "ulike-Swift.h"
@interface RegAccountView()

@property (strong, nonatomic) IBOutlet RegAccountView *view;
@property (strong,nonatomic) UIColor *squareColor;
@property (nonatomic) int level;
@property (strong, nonatomic) NSString * title;

@end

@implementation RegAccountView

-(UIColor*)squareBorderColor{
    if(!_squareColor){
        _squareColor = [ColorConvertor fromHex:@"CF4C50"];
    }
    return _squareColor;
}

-(void)awakeFromNib{
    [super awakeFromNib];
   // [self commonInit];
 // self.navTitle.text = _title;;
}
/*
-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self commonInit];
    }
    return self;
}
 */
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
  [self commonInit];
        
    }
    return self;
}

-(void)setLevel:(int)level title:(NSString*)title{
    self.view.navTitle.localizedText = title;
    switch (level) {
        case 1:
            [self.view.squareOne setBackgroundColor:self.squareBorderColor];
            self.view.bottomBorder.hidden = NO;
            break;
        case 2:
            [self.view.squareTwo setBackgroundColor:self.squareBorderColor];
              self.view.bottomBorderTwo.hidden = NO;
            break;
         case 3:
            [self.view.squareThree setBackgroundColor:self.squareBorderColor];
            break;
        default:
            break;
    }
}
-(void)commonInit{
    if(self.subviews.count > 0 ){
        return;
    }
    NSArray *loadedViews = [[NSBundle mainBundle] loadNibNamed:@"RegAccountBg" owner: self  options:nil];
    
    UIView *loadedSubview = [loadedViews firstObject];
    
    if(loadedSubview == nil)
        return;
     //   UIView *loadedSubview = [loadedViews firstObject];
        [self addSubview:loadedSubview];
        
        loadedSubview.translatesAutoresizingMaskIntoConstraints = NO;
    
        [self addConstraint:[self pin:loadedSubview attribute:NSLayoutAttributeTop]];
        [self addConstraint:[self pin:loadedSubview attribute:NSLayoutAttributeLeft]];
        [self addConstraint:[self pin:loadedSubview attribute:NSLayoutAttributeBottom]];
        [self addConstraint:[self pin:loadedSubview attribute:NSLayoutAttributeRight]];
}
- (NSLayoutConstraint *)pin:(id)item attribute:(NSLayoutAttribute)attribute
{
    return [NSLayoutConstraint constraintWithItem:self
                                        attribute:attribute
                                        relatedBy:NSLayoutRelationEqual
                                           toItem:item
                                        attribute:attribute
                                       multiplier:1.0
                                         constant:0.0];
}

@end


