//
//  ImageDownloader.h
//  uLike
//
//  Created by mac on 13/7/15.
//  Copyright (c) 2015 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol DownloaderDelegate <NSObject>

-(void)downloadTimeout;
-(void)downloadDone: (UIImage*)dImage;

@end

@interface ImageDownloader : NSObject
@property (strong, nonatomic) NSMutableArray * imageUrls;
@property (strong, nonatomic, readonly) NSMutableArray * imageCache;
-(id) initWithImageUrls: (NSArray*)urls imgSize:(CGSize) size;
@property (strong,nonatomic)  id<DownloaderDelegate>downloadDelegate;
-(void)fetchImagesAsync: (void(^)())complete timeOut:(void(^)())tout;

-(void)addUrl:(NSString*)url;

@end
