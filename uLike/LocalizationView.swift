import UIKit
extension UILabel{
    var localizedText: String{
        set(key){
            text = NSLocalizedString(key, comment: "")
        }
        get{
            return text!
        }
    }
}
extension UINavigationItem{
    var localizedText : String{
        set(key){
            self.title = NSLocalizedString(key, comment: "")
        }
        get{
            return title!
        }
    }
}
@IBDesignable class LocalizeNavigationItem : UINavigationItem{
    
   @IBInspectable  var localizeTitle: String = "" {
        didSet{
            #if TARGET_INTERFACE_BUILDER
            var bundle = NSBundle(forClass: self.dynamicType)
            self.title = bundle.localizedStringForKey(self.localizeTitle, value: "", table: nil)
            #else
                self.title = NSLocalizedString(self.localizeTitle, comment: "" )
            #endif
        }
    
    }
}

@IBDesignable class LocalizeTextView: UITextField{

    @IBInspectable  var localizePlaceHolder: String = ""{
        didSet{
            #if TARGET_INTERFACE_BUILDER
                var bundle = NSBundle(forClass: self.dynamicType)
                var title = bundle.localizedStringForKey(localizePlaceHolder, value: "", table: nil)
                self.placeholder = title
                #else
                var title =
                NSLocalizedString(localizePlaceHolder, comment: "" )
                self.placeholder = title
            #endif
        }
        
    }
    @IBInspectable  var holderColor : UIColor!{
        didSet{
            self.attributedPlaceholder =
                NSAttributedString(string: self.placeholder!, attributes: [NSForegroundColorAttributeName: holderColor])
        }
        
    }
    

}



@IBDesignable class LocalizeButton : UIButton{
    
    @IBInspectable  var localizeTitle: String = "" {
        didSet{
            #if TARGET_INTERFACE_BUILDER
                var bundle = NSBundle(forClass: self.dynamicType)
               var title = bundle.localizedStringForKey(self.localizeTitle, value: "", table: nil)
                   setTitle(title, forState: .Normal)
                   setTitle(title, forState: .Highlighted)
                #else
                     var title =
                    NSLocalizedString(self.localizeTitle, comment: "" )
                 setTitle(title, forState: .Normal)
                setTitle(title, forState: .Highlighted)
            #endif
        }
        
    }
}

@IBDesignable class LocalizeLabel : UILabel{
    
    @IBInspectable  var localizeTitle: String = "" {
        didSet{
            #if TARGET_INTERFACE_BUILDER
                var bundle = NSBundle(forClass: self.dynamicType)
                var title = bundle.localizedStringForKey(self.localizeTitle, value: "", table: nil)
             
                self.text = title
                #else
                var title =
                NSLocalizedString(self.localizeTitle, comment: "" )
              
                self.text = title
            #endif
        }
        
    }
    
    @IBInspectable  var labelColor: UIColor? {
        didSet{
                self.textColor = labelColor
        }
    }
    
}



extension UIButton{
    var localizedTitleNormal:String{
        
        set(key){
            setTitle(NSLocalizedString(key, comment: ""), forState: .Normal)
        }
        get{
            return titleForState(.Normal)!
        }
    }
    var localizedTitleHighlighted:String{
        set(key){
            setTitle(NSLocalizedString(key, comment: ""), forState: .Normal)
        }
        get{
            return titleForState(.Highlighted)!
        }
    }
}