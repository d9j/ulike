//
//  NSMutableArrayShuffling.m
//  uLike
//
//  Created by mac on 13/7/15.
//  Copyright (c) 2015 mac. All rights reserved.
//

#import "NSMutableArrayShuffling.h"

@implementation NSMutableArray(Shuffling)

-(void)shuffle{
    NSUInteger count = [self count];
    for (NSUInteger i = 0; i < count; ++i) {
        NSInteger remainingCount = count-i;
        
        NSUInteger exchangeInt = i+ arc4random_uniform((u_int32_t)remainingCount);
        [self exchangeObjectAtIndex:i withObjectAtIndex:exchangeInt];
    }
}

@end
