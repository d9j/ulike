//
//  FKDiskUtility.h
//  uLike
//
//  Created by mac on 16/7/15.
//  Copyright (c) 2015 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FKDiskUtility : NSObject
+(void)array: (NSMutableArray*) array addObject:(id) object;

+(NSString*)JSONStringForObject:(id) object error: (NSError * __autoreleasing*) errorRef
           invalidObjectHandler: (id(^)(id Object, BOOL *stop)) invalidObjectHandler;

@end
