//
//  MyScrollViewController.m
//  uLike
//
//  Created by mac on 19/7/15.
//  Copyright (c) 2015 mac. All rights reserved.
//

#import "MyScrollViewController.h"
@interface MyScrollViewController() <UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *myScrollView;
@property (weak, nonatomic) IBOutlet UILabel *myScrollLabel;
@property (weak, nonatomic) IBOutlet UIView *scollContentView;
@property (strong,nonatomic) NSMutableSet *visibleSet;
@property (strong,nonatomic) NSMutableSet *recycledSet;
@end

@implementation MyScrollViewController{
NSUInteger _scrollHeight;
NSUInteger _itemsPerPage;
NSUInteger _currentIndex;
NSUInteger _page;
NSUInteger _margin;
NSUInteger _itemsTotal;
CGRect _labelPos;
    
}

-(void)setUp{
    self.visibleSet = [ NSMutableSet set ];
    self.recycledSet = [ NSMutableSet set ];
}
-(void)displayNewItems: (NSUInteger*)num{
 

    
}
-(void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    self.myScrollLabel.hidden = YES;
    self.myScrollView.delegate = self;
    _labelPos = self.myScrollLabel.frame;
    _margin = _labelPos.size.height+10;
    _itemsPerPage = _myScrollView.frame.size.height/_margin;
    _scrollHeight = _margin*_itemsPerPage+2000; //50 def
    _itemsTotal = 0;
    
    for (int i = 0,j =0; i < _itemsPerPage; i++,j+=_margin) {
        UILabel *l = [self prepareLabel:j counter:i];
        [self.scollContentView addSubview:l];
        [_visibleSet addObject:l];
        _itemsTotal++;
    }
    self.myScrollView.contentSize = CGSizeMake(self.scollContentView.frame.size.width, _scrollHeight);
    NSLog(@"%lu",_scrollHeight );
}
-(UILabel*)prepareLabel: (NSUInteger)mar counter:(int)counter{
    CGRect mFrmPos = _labelPos;
    mFrmPos.origin.y += mar;
    UILabel * myLabel = [[UILabel alloc] initWithFrame:mFrmPos];
    myLabel.text = [NSString stringWithFormat:@"Label # %d",counter];
    return myLabel;
}

-(void)viewDidLoad{
    [super viewDidLoad];
   // self.myScrollLabel.hidden = YES;
    self.myScrollView.delegate = self;
    self.myScrollView.scrollEnabled = YES;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    _itemsTotal += 5;
    
    

}
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset NS_AVAILABLE_IOS(5_0){
    NSLog( @"changed %f",targetContentOffset->y/_margin-_itemsTotal);
}



@end
