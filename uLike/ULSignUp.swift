import Foundation

struct ULSignUp {
    let email : String
    let phone : String
    let password : String
    
    init(email: String, phone : String , password : String){
        self.email = email
        self.phone = phone
        self.password = password
    }

    func getQuery()-> NSDictionary{
        var query = [
            "user": [
                "phone" : self.phone,
                "email" :  self.email,
                "password" : self.password
            ]
        ]
        return query
    }
}