//
//  NSMutableArrayShuffling.h
//  uLike
//
//  Created by mac on 13/7/15.
//  Copyright (c) 2015 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray(Shuffling)

-(void) shuffle;

@end
