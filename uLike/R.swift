


 struct  R {
    struct Strings {
       static let alertNewName = "alert.new_name".local
       static let alertMessage = "alert.message".local
       static let alertSave = "alert.save".local
       static let alertCancel = "alert.cancel".local
    }
    
    
    struct Arrays {
        static let restaurantNames = ["Cafe Deadend", "Homei", "Teakha", "Cafe Loisl", "Petite Oyster",
            "For Kee Restaurant", "Po 's Atelier", "Bourke Street Bakery", "Haigh's Chocolate",
            "Espresso", "Upstate", "Traif", "Graham Avenue Meats", "Waffle & Wolf", "Five Leaves",
            "CafeLore", "Confessional", "Barrafina", "Donostia", "Royal Oak", "Thai Cafe"]
        
        static let restaurantImages = ["cafedeadend.jpg", "homei.jpg", "teakha.jpg", "cafeloisl.jpg",
            "petiteoyster.jpg", "forkeerestaurant.jpg", "posatelier.jpg", "bourkestreetbakery.jpg",
            "haighschocolate.jpg", "palominoespresso.jpg", "upstate.jpg", "traif.jpg",
            "grahamavenuemeats.jpg", "wafflewolf.jpg", "fiveleaves.jpg", "cafelore.jpg",
            "confessional.jpg", "barrafina.jpg", "donostia.jpg", "royaloak.jpg", "thaicafe.jpg"]
    }
    struct Id {
        static let cellId = "Cell"
    }
}