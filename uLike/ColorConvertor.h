//
//  ColorConvertor.h
//  uLike
//
//  Created by mac on 4/7/15.
//  Copyright (c) 2015 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ColorConvertor : NSObject

+(UIColor*)fromHex: (NSString*)hex;


@end
