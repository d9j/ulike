//
//  ImageDownloader.m
//  uLike
//
//  Created by mac on 13/7/15.
//  Copyright (c) 2015 mac. All rights reserved.
//

#import "ImageDownloader.h"
#import "ResizeableImage.h"
@interface ImageDownloader()

@property (strong, nonatomic,readwrite) NSMutableArray * imageCache;
@property CGSize imgSize;

@end

@implementation ImageDownloader


-(id) initWithImageUrls: (NSArray*)urls imgSize:(CGSize) size{
    self = [super init];
    if (self) {
        _imageUrls = [NSMutableArray arrayWithArray:urls];
        _imageCache = [[NSMutableArray alloc] init];
        _imgSize = size;
    }
    return self;
    
}
-(void)fetchImagesAsync: (void(^)())complete timeOut: (void(^)())tout{
    dispatch_group_t imageService =  dispatch_group_create();
    NSUInteger length =[_imageUrls count];
    __weak __typeof(self) weakSelf = self;
    dispatch_queue_t queue = dispatch_get_global_queue(0, 0);
    
    dispatch_time_t timeout = dispatch_time(DISPATCH_TIME_NOW, 5.0 * NSEC_PER_SEC);
    
    __block BOOL failedDownload = NO;
    
    dispatch_apply(length, queue,^(size_t i){
         dispatch_group_enter(imageService);
        failedDownload = NO;
        NSURL *imgUrl  = [ NSURL URLWithString:_imageUrls[i]];
        NSData *imageData = [ NSData dataWithContentsOfURL:imgUrl];
        typeof  (self) strngSelf = weakSelf;
        /*
        dispatch_after(timeout, dispatch_get_main_queue(), ^{
            if (imageData.length == 0) {
                failedDownload = YES;
            }
        });
        */
        
        
        /*
        if (failedDownload) {
            tout();
            return;
        }
        */
        dispatch_group_leave(imageService);
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImage *im = [UIImage imageWithData:imageData];
         //   [_downloadDelegate downloadDone:im];
            [strngSelf addImage:im];
        });
    });
    
    dispatch_group_notify(imageService, queue, ^{
      //  NSLog(@"Image Service Done!");
        complete();
    });
    dispatch_group_wait(imageService, timeout);
}
-(void)addImage:(UIImage*)img{
    [_imageCache addObject:  [img imageWithImage:_imgSize]];
}
-(void)addUrl:(NSString*)url{
    [_imageUrls addObject:url];
}






@end
