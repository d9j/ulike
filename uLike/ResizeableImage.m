//
//  RecizeableImage.m
//  uLike
//
//  Created by mac on 13/7/15.
//  Copyright (c) 2015 mac. All rights reserved.
//

#import "ResizeableImage.h"

@implementation UIImage(Resizeable)


-(UIImage*)imageWithImage: (CGSize)newSize{
    UIGraphicsBeginImageContext(newSize);
    [self drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


+(UIImage*)imageWithImage: (UIImage*)img size:(CGSize)newSize{
    UIGraphicsBeginImageContext(newSize);
    [img drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
@end
