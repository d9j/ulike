//
//  FKMacros.h
//  uLike
//
//  Created by mac on 9/7/15.
//  Copyright (c) 2015 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifdef __cplusplus
#define FK_EXTERN extern "C" __attribute__((visibility ("default")))
#else
#define FK_EXTERN extern __attribute__((visibility ("default")))
#endif
#define loggy(log) \
printf(log)