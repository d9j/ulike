//
//  AuthService.swift
//  uLike
//
//  Created by mac on 18/6/15.
//  Copyright (c) 2015 mac. All rights reserved.
//

import Foundation
import Alamofire
struct ULAuthService{
    
    private static let baseURL = "https://ulike.herokuapp.com/api/v1"
    
    private   enum ResourcePath : Printable{
        case Signup
        case Login
        var description: String{
            switch self{
            case .Login: return baseURL +   "/user/login"
            case .Signup: return baseURL +   "/user/create"
            }
    
        }
    }
    static func SignUp(signup : ULSignUp,
        response: (res : AuthData, merror: String) -> () ) {
            
            var url = ResourcePath.Signup.description
        Alamofire.request(.POST, url, parameters: signup.getQuery() as? [String : AnyObject], encoding: ParameterEncoding.JSON)
            .responseJSON(options: NSJSONReadingOptions.MutableContainers) {
                (request : NSURLRequest, resp:  NSHTTPURLResponse?, result, error : NSError?) -> Void in
                var  errors : NSArray = JsonParser.parseErrors((result as? NSDictionary)!)
                var authData = AuthData()
                if(errors.count > 0){
                       response(res: authData , merror: errors[0][0] as! String)
                       return
                }
                
                
          }
        
    }
    
}