import UIKit

class Background: UIView {

    
    @IBOutlet var mainBg: UIView!
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let mainBundle  = NSBundle.mainBundle()
        let loadedViews : NSArray = mainBundle.loadNibNamed("BgView", owner: self, options: nil)
        let loadedSubView = loadedViews.firstObject as! UIView
        
        
        self.addSubview(loadedSubView)
        
        
        loadedSubView.setTranslatesAutoresizingMaskIntoConstraints(false)
        
        self.addConstraint(pin(loadedSubView, attribute: NSLayoutAttribute.Top))
        
           self.addConstraint(pin(loadedSubView, attribute: NSLayoutAttribute.Left))
        
           self.addConstraint(pin(loadedSubView, attribute: NSLayoutAttribute.Bottom))
        
           self.addConstraint(pin(loadedSubView, attribute: NSLayoutAttribute.Right))
    }
    
    private
    func pin( item: AnyObject, attribute: NSLayoutAttribute) ->NSLayoutConstraint{
        
      var constraint =  NSLayoutConstraint(item: self, attribute: attribute, relatedBy: NSLayoutRelation.Equal, toItem: item, attribute: attribute, multiplier: 1.0, constant: 0.0)
        
        return constraint
        
    }
    

    
}
