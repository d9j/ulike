//
//  JsonParser.swift
//  uLike
//
//  Created by mac on 20/6/15.
//  Copyright (c) 2015 mac. All rights reserved.
//

import Foundation


class JsonParser {
    
    
    static func  parseErrors(data: NSDictionary) -> NSArray{
        var e = []
        if let errors = data["errors"] as? NSDictionary{
            e =  errors.allValues
        }
        return e
    }
    
    static func parseAuthData(data: NSDictionary) -> AuthData{
        
        var authData = AuthData()
        
        if let hash_dic = data["hash"] as? NSDictionary{
            authData.client_hash = hash_dic["client_hash"] as! String
            authData.private_hash = hash_dic["private_hash"] as! String
        }
        if let user = data["user"] as? NSDictionary{
            authData.phone = user["phone"] as! String
            authData.email = user["email"] as! String
        }
        return authData
    }

    
}