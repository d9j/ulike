//
//  MySingleTone.h
//  uLike
//
//  Created by mac on 16/7/15.
//  Copyright (c) 2015 mac. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef NS_ENUM(NSUInteger, FFLogFlush){
    
    FFLogged,
    FFStopped
};

extern NSString * const qw;


@interface MySingleton : NSObject

+(instancetype) singleton;

-(void)logAction: (NSString*)action;

@end
