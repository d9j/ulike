//
//  RecizeableImage.h
//  uLike
//
//  Created by mac on 13/7/15.
//  Copyright (c) 2015 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface UIImage(Resizeable)


-(UIImage*)imageWithImage: (CGSize)newSize;
+(UIImage*)imageWithImage: (UIImage*)img size:(CGSize)newSize;


@end
