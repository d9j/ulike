//
//  CreateProfileViewController.m
//  uLike
//
//  Created by mac on 3/7/15.
//  Copyright (c) 2015 mac. All rights reserved.
//

#import "CreateProfileViewController.h"
#import "NSMutableArrayShuffling.h"
#import "RegAccountView.h"
#import "ImageDownloader.h"
#import "ulike-Swift.h"
#import "MySingleton.h"
#import "MyTestSegueViewController.h"

NSString *const setImageNotif = @"setImageNotification";
static NSString * const customSegue = @"customSegue";

@interface CreateProfileViewController() <DownloaderDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *downloadedImage;

@property (weak, nonatomic) IBOutlet RegAccountView *navView;

@property (strong, nonatomic) dispatch_source_t timerSource;

@property (strong,nonatomic) ImageDownloader * imageDownloader;
@end

@implementation CreateProfileViewController
-(void)downloadTimeout{
    
}
-(void)downloadDone: (UIImage*)dImage{
    NSLog(@"Download Done");
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier  isEqual: @"customSegue"]) {
        MyTestSegueViewController * controller = (MyTestSegueViewController*)segue.destinationViewController;
        controller.resultSeg = (NSString*)sender;
    }
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setImage:) name:setImageNotif object:nil ];
    }
    return self;
}

- (IBAction)clickMeCaller {
  //  [[MySingleton singleton] logAction:@"Clicked Caller"];
    [self performSegueWithIdentifier:customSegue sender:@"Hi from Create Profile"];
    
}
-(void)viewDidLoad{
    [super viewDidLoad];
  NSArray *  imageUrlSet = [[NSArray alloc] initWithObjects:
                                                   @"http://i.imgur.com/WFNYvuB.jpg",
                                                   @"http://i.imgur.com/GsnTewp.jpg",
                                                   @"http://i.imgur.com/943d0yH.jpg",
                                                   @"http://i.imgur.com/fFhGcn5.png"
                 
                 ,nil];
    
    
    /*
    _imageDownloader = [[ImageDownloader alloc] initWithImageUrls:imageUrlSet imgSize:self.downloadedImage.frame.size];
    _imageDownloader.downloadDelegate = self;
    [self.navView setLevel:2 title:  @"nav.createProfile" ];
    __weak __typeof(self) wself = self;
    [self.imageDownloader fetchImagesAsync:^{
      [wself imageQue];
        
    } timeOut:^{}];
   */

    //[self.navContainer addSubview:self.navView];
    
    [MySingleton singleton];
}
-(void)imageQue{

    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
   self.timerSource = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    
     // __weak __typeof(self) wself = self;
    
    
    dispatch_source_set_timer(self.timerSource , DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC, 1 * NSEC_PER_SEC);
    dispatch_source_set_timer(self.timerSource, dispatch_walltime(NULL, 0), 2ull * NSEC_PER_SEC, 1ull * NSEC_PER_SEC);

   //[arraySet shuffle];
    
    //[arrIndex shuffle];
    __block NSInteger p = 0;
    __block BOOL countingBack = NO;
    __weak __typeof(self) wself = self;
    __weak __typeof(self.imageDownloader.imageCache) imgCache = self.imageDownloader.imageCache;
    dispatch_source_set_event_handler(self.timerSource , ^{
        p += p < imgCache.count-1 && !countingBack;
    
        
        countingBack = p > 0 ? p-- : NO;

        typeof (self)stngSelf = wself;
        dispatch_async(dispatch_get_main_queue(), ^{
                stngSelf.downloadedImage.image = [imgCache objectAtIndex:p];
        });
    });
    dispatch_resume(self.timerSource);
}
-(void)awakeFromNib{
    [super awakeFromNib];
}

@end
