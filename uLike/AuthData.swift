//
//  AuthData.swift
//  uLike
//
//  Created by mac on 20/6/15.
//  Copyright (c) 2015 mac. All rights reserved.
//

import Foundation

class AuthData{

    var email : String = ""
    var phone : String = ""
    var client_hash : String = ""
    var private_hash : String = ""
    init(){
        
    }
    init(email: String, phone: String, client_hash: String, private_hash : String){
        self.email = email
        self.phone = phone
        self.client_hash = client_hash
        self.private_hash = private_hash
    }
    
}