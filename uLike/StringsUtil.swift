

import Foundation


extension String{
    var isBlank: Bool{
        get{
            let trimmed = stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
            return trimmed.isEmpty
        }
    }
    var local : String{
        get{
            let translated = NSLocalizedString(self, tableName: nil, comment: "")
            return translated
        }
        
    }
}
