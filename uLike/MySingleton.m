//
//  MySingleTone.m
//  uLike
//
//  Created by mac on 16/7/15.
//  Copyright (c) 2015 mac. All rights reserved.
//

#import "MySingleton.h"

NSString *const qw  = @"logged";

@implementation MySingleton


+(instancetype) singleton{
    static dispatch_once_t pred;
    
    static MySingleton *shared = nil;
    
    dispatch_once(&pred,^{
    
        shared = [[MySingleton alloc] init];
    });
    return shared;
}
-(id) init{
    self = [super init];
    if (self) {
        [self addObservers];
    }
    return self;
}

-(void)loggedNotif{
    NSLog(@"Logged Notif");
}

-(void)logAction: (NSString*)action{
  NSLog(@"%@", action);
    [[NSNotificationCenter defaultCenter]postNotificationName:qw object:self];
    
}
-(void)addObservers{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loggedNotif) name:qw object:NULL];
     }
-(void)logAction{
    NSLog(@"Log Action!");
}


@end
