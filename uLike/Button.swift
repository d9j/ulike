import UIKit

extension UIButton {
    
    var colorize : UIColor{
        get{
            return self.titleColorForState(.Highlighted)!
        }
        set(value){
            self.setTitleColor(value, forState: .Highlighted)
        }
    }
}

