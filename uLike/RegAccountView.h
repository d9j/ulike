//
//  RegAccountView.h
//  uLike
//
//  Created by mac on 3/7/15.
//  Copyright (c) 2015 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegAccountView : UIView

-(UIColor*)squareBorderColor;


/*
-(void)setLevel:(int)level title:(NSString*)title;

*/
@property (weak, nonatomic) IBOutlet UIView *squareOne;

@property (weak, nonatomic) IBOutlet UIView *squareTwo;


@property (weak, nonatomic) IBOutlet UIView *squareThree;
@property (weak, nonatomic) IBOutlet UIView *bottomBorderTwo;

@property (weak, nonatomic) IBOutlet UIView *bottomBorder;

//- (instancetype)initWithCoder:(NSCoder *)aDecoder;

@property (weak, nonatomic) IBOutlet UILabel *navTitle;

-(void)setLevel:(int)level title:(NSString*)title;

@end
