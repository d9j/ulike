//
//  AccountViewController.swift
//  uLike
//
//  Created by mac on 9/6/15.
//  Copyright (c) 2015 mac. All rights reserved.
//

import UIKit

class RegisterAccountViewController: UIViewController {
    
    @IBOutlet weak var emailField: LocalizeTextView!
    
    @IBOutlet weak var phoneField: LocalizeTextView!
    @IBOutlet weak var passwordField: LocalizeTextView!
    
    @IBOutlet weak var disableButtonView: UIView!
    
    //Objective C class
    
    @IBOutlet weak var navView: RegAccountView!
    
    
    @IBOutlet weak var continueButton: LocalizeButton!{
        didSet{
            continueButton.addTarget(self, action: Selector("continueButtonPressed:"), forControlEvents: UIControlEvents.TouchUpInside)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        navView.setLevel(1, title:  "account.signMeUp")
        addEditedTarget(emailField)
        addEditedTarget(phoneField)
        addEditedTarget(passwordField)
        toggleButtonState(true)
    }
    
    func addEditedTarget(textField : UITextField){
        
        textField.addTarget(self, action: Selector("editedField:"), forControlEvents: UIControlEvents.EditingChanged)
    }
    
    func editedField(sender: UITextField) {
        
        let empty: Bool =
            emailField.text.isEmpty
            || phoneField.text.isEmpty
            || passwordField.text.isEmpty
        
        toggleButtonState(empty)
    }
    func toggleButtonState(disable: Bool){
        continueButton.enabled = !disable
        disableButtonView.hidden = !disable
    }
    func continueButtonPressed(sender : AnyObject?){
        
        let signup  = ULSignUp(email: emailField.text, phone: phoneField.text, password: passwordField.text)
    
        ULAuthService.SignUp(signup, response: { (res, merror) -> () in
            println(merror)
            
        })
        
        
    }

}
