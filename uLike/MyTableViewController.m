//
//  MyTableViewController.m
//  uLike
//
//  Created by mac on 20/7/15.
//  Copyright (c) 2015 mac. All rights reserved.
//

#import "MyTableViewController.h"
#import "MainCell.h"

@interface MyTableViewController() <UITableViewDelegate,UITableViewDataSource, MainCellDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
@implementation MyTableViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 120u;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MainCell * cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    cell.cellIndex = indexPath;
    cell.delegate = self;
    return cell;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(MainCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    cell.firstLabel.text = [NSString stringWithFormat:@"FirstLabel %d",(int)indexPath.row];
    cell.secondLabel.text = [NSString stringWithFormat:@"SecondLabel %d",(int)indexPath.row];
}


-(void)didClickOnButtonWithIndex: (NSIndexPath*)index  withData:(id)sender{
    
    UIAlertController * alertContrller = [UIAlertController alertControllerWithTitle:@"Edit Cell Title" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    __weak typeof(self) wself = self;
    
    UIAlertAction * cancelaction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        
    }];
    UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
       UITextField * cellTitle = alertContrller.textFields.firstObject;
        
        [wself editCell:cellTitle.text indexPath:index];
    }];
    [alertContrller addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Title..";
    }];
    [alertContrller addAction:cancelaction];
    [alertContrller addAction:okAction];
    [self presentViewController:alertContrller animated:YES completion:^{
    }
     ];
}
-(void)editCell:(NSString*)title  indexPath: (NSIndexPath*) index {
    
    MainCell  *cell = (MainCell*)[self.tableView cellForRowAtIndexPath: index ];
    cell.firstLabel.text = title;

    
}

@end
