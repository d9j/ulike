//
//  MainCell.h
//  uLike
//
//  Created by mac on 20/7/15.
//  Copyright (c) 2015 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MainCellDelegate <NSObject>

-(void)didClickOnButtonWithIndex: (NSIndexPath*)index  withData:(id)sender;
@end
@interface MainCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *mImage;
@property (nonatomic,weak) id<MainCellDelegate> delegate;
@property (weak,nonatomic) NSIndexPath * cellIndex;

@property (weak, nonatomic) IBOutlet UILabel *firstLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondLabel;

@end
